package org.hewitt.actor;

import java.util.Optional;

public interface Acquaintances<R> {

	public R self();
	
	public R sender();
	
	public R system();
	
	public Optional<R> acq(String name);
}
