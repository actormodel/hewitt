package org.hewitt.actor;

public interface Actor<R extends Reference> {

	public void send(R target, Messenger messenger);
	
	public void become(Object...behavior);
}
