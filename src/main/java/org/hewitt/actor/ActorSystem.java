package org.hewitt.actor;

public interface ActorSystem<R extends Reference, A extends Actor<R>> {

	public R create(Object address, A instance);

}
