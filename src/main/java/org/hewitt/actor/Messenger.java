package org.hewitt.actor;

public interface Messenger {

	public void process();
	
	public void arrival(Messenger messenger);
	
	public void activate(Messenger messenger);
	
	public MessageSequence and(Messenger messenger);
}
